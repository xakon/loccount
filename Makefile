# Makefile for loccount
# You must have the Go compiler and tools installed to build this software.
# You will also need Ptthon installed to build it, though not to run it.
# You will need nodejs installed to run the tests.

VERS=$(shell sed -n <NEWS.adoc '/^[0-9]/s/:.*//p' | head -1)

GOFLAGS=-ldflags='-X main.version=$(VERS)'
loccount: loccount.go traits.json tablegen.py
	-test -f go.mod || go mod init loccount
	go generate
	go build $(GOFLAGS)

clean:
	go clean
	rm -f *.html *.1 *~ tests/*~ traits.go

install: loccount
	go install



# For a serious torture test (75 languages!)
# git clone git@github.com:kanaka/mal.git tests/mal
# and then run this.
MAL=tests/mal/
check: loccount
	@-pylint --score=n tablegen.py
	@./loccount -s >/dev/null
	@(./loccount -x $(MAL) -i tests; ./loccount -x $(MAL) -u tests) | diff -u check.good -
	@(if [ -x $(MAL) ]; then ./loccount -x "$(MAL)/?tests/step[01]_" $(MAL) >/dev/null; LOCCOUNT=./loccount ./tests/check_mal.js $(MAL); fi)
	@echo "No check output (other than this line) is good news"

# If loccount reports that a mal implementation directory has <= 5 files
# (some are misc support files) then it isn't finding that
# implementation language at all.
check-mal-missing: loccount $(MAL)
	@VERBOSE=1 LOCCOUNT=./loccount ./tests/check_mal.js $(MAL) 2>/dev/null | grep "GOOD"  | sed 's/.* \([^\/]*\)\/.*/\1/' | sort | uniq -c | grep "\<[0-9]\>" || echo "Coverage complete."


testbuild: loccount
	@rm -f tests/*~; (./loccount -x $(MAL) -i tests; ./loccount -x $(MAL) -u tests) >check.good

gofmt:
	gofmt -s -d loccount.go | patch -p0

# In case the logo ever needs to be scaled, the SVG original can be found here:
# https://commons.wikimedia.org/wiki/File:Tally_marks-Five-bar_Gate.svg
SOURCES = README.adoc COPYING NEWS.adoc control loccount.go traits.json tablegen.py \
		loccount.adoc hacking.adoc design-notes.adoc references.adoc \
		Makefile TODO loccount-logo.png check.good tests/

# Note: to suppress the footers with timestamps being generated in HTML,
# we use "-a nofooter".
# To debug asciidoc problems, you may need to run "xmllint --nonet --noout --valid"
# on the intermediate XML that throws an error.
.SUFFIXES: .html .adoc .1

.adoc.1:
	asciidoctor -D. -a nofooter -b manpage $<
.adoc.html:
	asciidoctor -D. -a nofooter -a webfonts! $<

version:
	@echo $(VERS)

# You can profile this code with, e.g.
#
# loccount -cpuprofile /tmp/loc.prof .
# go tool pprof loccount /tmp/loc.prof
#
# "top20" is a useful command to start with in viewing results.
#
# Also, remember to check the report card occasionally:
# https://goreportcard.com/report/gitlab.com/esr/loccount#L1

# Report which languages lack lloc support
sloc: loccount
	./loccount -i -s  | sort >/tmp/sloc$$; ./loccount -i -l | sort >/tmp/lloc$$
	comm -23 /tmp/sloc$$ /tmp/lloc$$
	rm -f /tmp/sloc$$ /tmp/lloc$$

loccount-$(VERS).tar.gz: $(SOURCES) loccount.1
	tar --transform='s:^:loccount-$(VERS)/:' --show-transformed-names -czf loccount-$(VERS).tar.gz $(SOURCES) loccount.1

dist: loccount-$(VERS).tar.gz

release: loccount-$(VERS).tar.gz loccount.html hacking.html
	shipper version=$(VERS) | sh -e -x

refresh: loccount.html
	shipper -N -w version=$(VERS) | sh -e -x
